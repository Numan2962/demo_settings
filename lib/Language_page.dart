import 'package:flutter/material.dart';

class LanguagePage extends StatelessWidget {
  static const routeName = '/language-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Language"),
        backgroundColor: Theme.of(context).accentColor,
      ),
    );
  }
}
