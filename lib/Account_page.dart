import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class AccountPage extends StatelessWidget {
  static const routeName = '/account-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Account"),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            titlePadding: EdgeInsets.all(15),
            tiles: [
              SettingsTile(
                title: 'Change Number',
                leading: Icon(Icons.change_circle),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: 'Delete Account',
                leading: Icon(Icons.delete),
                onPressed: (BuildContext context) {},
              ),
            ],
          ),
          SettingsSection(
            titlePadding: EdgeInsets.all(15),
            title: "Remove Tasks Older than",
            tiles: [
              SettingsTile(
                title: '1 week',
                leading: Icon(Icons.calendar_view_week),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: '1 month',
                leading: Icon(Icons.calendar_view_month_rounded),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: '1 year',
                leading: Icon(Icons.calendar_view_month_outlined),
                onPressed: (BuildContext context) {},
              ),
            ],
          )
        ],
      ),
    );
  }
}
