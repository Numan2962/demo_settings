import 'package:demo_settings/Appearance.dart';
import 'package:flutter/material.dart';

import 'Account_page.dart';
import 'Language_page.dart';
import 'Linked_devices.dart';
import 'Notification_page.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 50),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: Container(
                    width: 350,
                    height: 130,
                    color: Colors.blueGrey[900],
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                      child: Column(children: <Widget>[
                        CircleAvatar(
                          child: ClipOval(
                            child: Image.asset(
                              'assets/images/numan.jpg',
                              height: 100,
                              width: 150,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Column(children: <Widget>[
                          Text(
                            'Abdullah Al Numan',
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            'Software Engineer',
                            style: TextStyle(color: Colors.white70),
                          ),
                        ]),
                        Container(
                          alignment: Alignment.center,
                          child: Icon(Icons.edit, color: Colors.white70),
                        ),
                      ]),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Column(children: <Widget>[
              ListTile(
                title: Text(
                  'Grinz Web',
                  style: TextStyle(color: Colors.orange[50]),
                ),
                subtitle: Text(
                  'QR code',
                  style: TextStyle(color: Colors.white70),
                ),
                leading: Icon(Icons.language, color: Colors.white70),
              ),
              ListTile(
                title: Text(
                  'Group List',
                  style: TextStyle(color: Colors.orange[50]),
                ),
                leading: Icon(Icons.group, color: Colors.white70),
              ),
              ListTile(
                title: Text(
                  'Appearance',
                  style: TextStyle(color: Colors.orange[50]),
                ),
                subtitle: Text(
                  'Theme, Task Background, Chat Background',
                  style: TextStyle(color: Colors.white70),
                ),
                leading: Icon(Icons.animation, color: Colors.white70),
                onTap: () =>
                    Navigator.pushNamed(context, AppearancePage.routeName),
              ),
              ListTile(
                title: Text(
                  'Linked Devices',
                  style: TextStyle(color: Colors.orange[50]),
                ),
                leading: Icon(Icons.devices, color: Colors.white70),
                onTap: () =>
                    Navigator.pushNamed(context, LinkedDevicesPage.routeName),
              ),
              ListTile(
                title: Text(
                  'Language',
                  style: TextStyle(color: Colors.orange[50]),
                ),
                subtitle: Text(
                  'English US',
                  style: TextStyle(color: Colors.white70),
                ),
                leading: Icon(Icons.language, color: Colors.white70),
                onTap: () =>
                    Navigator.pushNamed(context, LanguagePage.routeName),
              ),
              ListTile(
                title: Text(
                  'Account',
                  style: TextStyle(color: Colors.orange[50]),
                ),
                subtitle: Text(
                  'Privacy, security, change number',
                  style: TextStyle(color: Colors.white70),
                ),
                leading: Icon(Icons.attribution, color: Colors.white70),
                onTap: () =>
                    Navigator.pushNamed(context, AccountPage.routeName),
              ),
              ListTile(
                title: Text(
                  'Notification',
                  style: TextStyle(color: Colors.orange[50]),
                ),
                subtitle: Text(
                  'Task, Message',
                  style: TextStyle(color: Colors.white70),
                ),
                leading: Icon(Icons.notifications, color: Colors.white70),
                onTap: () =>
                    Navigator.pushNamed(context, NotificationPage.routeName),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
