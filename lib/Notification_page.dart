import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class NotificationPage extends StatelessWidget {
  static const routeName = '/notification-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notification"),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            titlePadding: EdgeInsets.all(15),
            title: 'Task',
            tiles: [
              SettingsTile(
                title: 'Notification Tune',
                leading: Icon(Icons.tune),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: 'Vibrate',
                leading: Icon(Icons.vibration),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: 'Reset',
                leading: Icon(Icons.reset_tv_rounded),
                onPressed: (BuildContext context) {},
              ),
            ],
          ),
          SettingsSection(
            titlePadding: EdgeInsets.all(15),
            title: 'Message',
            tiles: [
              SettingsTile(
                title: 'Notification Tune',
                leading: Icon(Icons.tune),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: 'Vibrate',
                leading: Icon(Icons.vibration),
                onPressed: (BuildContext context) {},
              ),
              SettingsTile(
                title: 'Reset',
                leading: Icon(Icons.reset_tv_rounded),
                onPressed: (BuildContext context) {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}
