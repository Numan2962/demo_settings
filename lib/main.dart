import 'package:demo_settings/Account_page.dart';
import 'package:demo_settings/Appearance.dart';
import 'package:demo_settings/Notification_page.dart';
import 'package:demo_settings/settings_screen.dart';
import 'package:flutter/material.dart';

import 'Language_page.dart';
import 'Linked_devices.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Settings',
      theme: ThemeData(
        primarySwatch: Colors.brown,
        accentColor: Colors.black,
        canvasColor: Colors.black,
      ),

      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => Settings(),
        AppearancePage.routeName: (ctx) => AppearancePage(),
        AccountPage.routeName: (ctx) => AccountPage(),
        NotificationPage.routeName: (ctx) => NotificationPage(),
        LanguagePage.routeName: (ctx) => LanguagePage(),
        LinkedDevicesPage.routeName: (ctx) => LinkedDevicesPage(),
      },
    );
  }
}
