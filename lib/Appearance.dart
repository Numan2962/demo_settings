import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class AppearancePage extends StatelessWidget {
  static const routeName = '/appearance-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Appearance"),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            titlePadding: EdgeInsets.all(12),
            title: 'Theme',
            tiles: [
              SettingsTile.switchTile(
                title: 'Default',
                leading: Icon(Icons.add_reaction),
                switchValue: true,
                onToggle: (value) {},
              ),
              SettingsTile.switchTile(
                title: 'Light',
                leading: Icon(Icons.light_mode),
                switchValue: true,
                onToggle: (value) {},
              ),
              SettingsTile.switchTile(
                title: 'Dark',
                leading: Icon(Icons.dark_mode),
                switchValue: true,
                onToggle: (value) {},
              ),
            ],
          ),
          SettingsSection(
            titlePadding: EdgeInsets.all(12),
            title: 'Task Background',
            tiles: [
              SettingsTile.switchTile(
                title: 'Dim When Dark Mode',
                leading: Icon(Icons.dark_mode_rounded),
                switchValue: true,
                onToggle: (value) {},
              ),
              SettingsTile.switchTile(
                title: 'Grinz Pattern',
                leading: Icon(Icons.fingerprint),
                switchValue: true,
                onToggle: (value) {},
              ),
              SettingsTile(
                title: 'Reset',
                leading: Icon(Icons.reset_tv),
                onPressed: (BuildContext context) {},
              ),
            ],
          ),
          SettingsSection(
            titlePadding: EdgeInsets.all(12),
            title: 'Chat Background',
            tiles: [
              SettingsTile.switchTile(
                title: 'Dim When Dark Mode',
                leading: Icon(Icons.dark_mode_outlined),
                switchValue: true,
                onToggle: (value) {},
              ),
              SettingsTile.switchTile(
                title: 'Grinz Pattern',
                leading: Icon(Icons.fingerprint),
                switchValue: true,
                onToggle: (value) {},
              ),
              SettingsTile(
                title: 'Reset',
                leading: Icon(Icons.reset_tv),
                onPressed: (BuildContext context) {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}
