import 'package:flutter/material.dart';

class LinkedDevicesPage extends StatelessWidget {
  static const routeName = '/linked_devices-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Linked Devices"),
        backgroundColor: Theme.of(context).accentColor,
      ),
    );
  }
}
